from django.db import models

# Create your models here.
class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    profile_id = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

class SteamObject(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)

class ObjectColor(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)

class ExcludedObject(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)

class FoundObject(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    object_name = models.CharField(max_length=200)
    object_color = models.CharField(max_length=200, null=True)
    user_name = models.CharField(max_length=200)
    user_id = models.CharField(max_length=200)

    class Meta:
        unique_together = ('user_name', 'object_color', 'object_name')
