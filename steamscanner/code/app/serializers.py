from rest_framework import serializers

from .models import Profile, SteamObject, ObjectColor, ExcludedObject, FoundObject

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = "__all__"

class SteamObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = SteamObject
        fields = "__all__"

class ObjectColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjectColor
        fields = "__all__"

class ExcludedObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExcludedObject
        fields = "__all__"

class FoundObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoundObject
        fields = "__all__"
