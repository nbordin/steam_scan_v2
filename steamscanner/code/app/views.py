from rest_framework import generics
from django.http import Http404
from rest_framework import status
from rest_framework.response import Response

from .models import Profile, SteamObject, ObjectColor, ExcludedObject, FoundObject
from .serializers import ProfileSerializer, SteamObjectSerializer, ObjectColorSerializer, ExcludedObjectSerializer, FoundObjectSerializer

def _delete(model, id):
    try:
        elm = model.objects.get(id=id)
    except model.DoesNotExist as e:
        raise Http404
    elm.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

class ProfileAPIView(generics.ListCreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def delete(self, request, id):
        return _delete(Profile, id)

class SteamObjectAPIView(generics.ListCreateAPIView):
    queryset = SteamObject.objects.all()
    serializer_class = SteamObjectSerializer

    def delete(self, request, id):
        return _delete(SteamObject, id)

class ObjectColorAPIView(generics.ListCreateAPIView):
    queryset = ObjectColor.objects.all()
    serializer_class = ObjectColorSerializer

    def delete(self, request, id):
        return _delete(ObjectColor, id)

class ExcludedObjectAPIView(generics.ListCreateAPIView):
    queryset = ExcludedObject.objects.all()
    serializer_class = ExcludedObjectSerializer

    def delete(self, request, id):
        return _delete(ExcludedObject, id)

class FoundObjectAPIView(generics.ListCreateAPIView):
    queryset = FoundObject.objects.all()
    serializer_class = FoundObjectSerializer
