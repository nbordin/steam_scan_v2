import datetime
import json
import time
import requests

from django.db.utils import IntegrityError
from django.utils.timezone import make_aware
from app.models import Profile, SteamObject, ObjectColor, ExcludedObject, FoundObject

def log_load(name = 'steam_scan'):
    import logging
    from logging import StreamHandler
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    sh = StreamHandler()
    formatter = '[%(asctime)s][%(process)d][%(levelname)s][' + name + '] %(message)s'
    formatter = logging.Formatter(formatter)
    sh.setFormatter(formatter)
    log.addHandler(sh)
    return log

URL = 'https://steamcommunity.com/inventory/{}/440/2?l=english&count=5000'
def table_to_list(model):
    return [x for x in model.objects.all().values()]

log = log_load()

while True:

    elements = []

    log.info('Getting data from DB')
    USERS       = table_to_list(Profile)
    OBJECTS     = [x['name'] for x in table_to_list(SteamObject)]
    EXCLUDED    = [x['name'] for x in table_to_list(ExcludedObject)]
    COLORS      = [x['name'].replace(' ', '').lower() for x in table_to_list(ObjectColor)]
    log.info('DB data OK')

    for user in USERS:
        user['url'] = URL.format(user['profile_id'])

    for user in USERS:

        username = user['name']
        user_id = user['profile_id']

        log.info('Scanning user {}'.format(username))

        try:
            r   = requests.get(user['url'], timeout = 10)
        except Exception as e:
            log.debug('Error in request ' + str(e))
            continue

        if not r.ok:
            log.debug('Error in request for ' + username)
            continue

        data = r.json()
        descriptions = data['descriptions']

        for description in descriptions:

            name    = description.get('name')

            if not name:
                continue

            if name in OBJECTS:
                log.info('OGGETTO: ' + name + ' | UTENTE: ' + user['name'])
                elements.append({
                    'object_name'   : name,
                    'object_color'  : 'No color',
                    'user_name'     : username,
                    'user_id'       : user_id,
                })

            object_dscs  = description.get('descriptions', [])
            for object_dsc in object_dscs:

                if object_dsc.get('color'):

                    color = object_dsc.get('value')

                    if color:

                        rndm = color.replace('Paint Color: ', '').replace(' ', '').lower()
                        if rndm in COLORS and not name in EXCLUDED:
                            log.info(
                                'OGGETTO: ' + name + ' COLORE: ' + color + \
                                ' UTENTE: ' + username
                            )
                            elements.append({
                                'object_name'   : name,
                                'object_color'  : color,
                                'user_name'     : username,
                                'user_id'       : user_id,
                            })
                            break

        log.info('Scanning user {} OK'.format(username))

    last_10_minutes = datetime.datetime.utcnow() - datetime.timedelta(minutes=10)
    FoundObject.objects.filter(date__lt=make_aware(last_10_minutes)).delete()

    FoundObject.objects.all().delete()
    for element in elements:
        found_object = FoundObject(**element)
        try:
            found_object.save()
        except IntegrityError as e:
            pass

    time.sleep(10)
