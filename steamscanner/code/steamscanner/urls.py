from django.urls import path
from django.conf.urls import url
import app.views as app_views

urlpatterns = [
    path(r'api/profile', app_views.ProfileAPIView.as_view(), name='profile'),
    url(r'api/profile/(?P<id>[0-9]+)/$', app_views.ProfileAPIView.as_view(), name='profile-filter'),
    path(r'api/steam-object', app_views.SteamObjectAPIView.as_view(), name='steam-object'),
    url(r'api/steam-object/(?P<id>[0-9]+)/$', app_views.SteamObjectAPIView.as_view(), name='steam-object-filter'),
    path(r'api/object-color', app_views.ObjectColorAPIView.as_view(), name='object-color'),
    url(r'api/object-color/(?P<id>[0-9]+)/$', app_views.ObjectColorAPIView.as_view(), name='object-color-filter'),
    path(r'api/excluded-object', app_views.ExcludedObjectAPIView.as_view(), name='excluded-object'),
    url(r'api/excluded-object/(?P<id>[0-9]+)/$', app_views.ExcludedObjectAPIView.as_view(), name='excluded-object-filter'),
    path(r'api/found-object', app_views.FoundObjectAPIView.as_view(), name='found-object')
]
