import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import '@/scss/globals.scss'

Vue.use(Buefy, {
    defaultIconPack: 'fas'
})
axios.defaults.baseURL = 'http://localhost/api/'
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
