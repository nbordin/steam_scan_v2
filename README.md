First installation:

```sh
cd /path/to/folder
docker-compose build
```

To run:

```sh
cd /path/to/folder
docker-compose up
```

To reach the homepage: localhost/home
